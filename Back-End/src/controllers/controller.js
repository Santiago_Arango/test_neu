const { response } = require('express')
const { Pool} = require('pg')

const pool = new Pool({
    host:'localhost',
    user : 'postgres',
    password:'Santiago3101',
    database:'test',
    port:'5432'

})


const getByPais = async (req,res) => {
    const nombre_pais = req.params.nombre_pais;
    await pool.query('SELECT * FROM public."consolidados" WHERE (consolidados.nombre_pais = $1) ORDER BY id DESC',[nombre_pais])
    .then(data => {
        res.status(200).json(data.rows)
    })
    .catch(err => {
        res.status(500).send({  message: "Error en la consulta del por pais =" + nombre_pais + " error:" + err});
    });
}

const getByEstado = async (req,res) => {
    const nombre_estado = req.params.nombre_estado;
    await pool.query('SELECT * FROM public."consolidados" WHERE (consolidados.nombre_estado = $1) ORDER BY id DESC', [nombre_estado])
    .then(data => {
        res.status(200).json(data.rows)
    })
    .catch(err => {
        res.status(500).send({  message: "Error en la consulta del por estado =" + nombre_estado + " error:" + err});
    });
}

const getByCiudad = async (req,res) => {
    const nombre_ciudad = req.params.nombre_ciudad;
    await pool.query('SELECT * FROM public."consolidados" WHERE (consolidados.nombre_ciudad = $1) ORDER BY id DESC',[nombre_ciudad])
    .then(data => {
        res.status(200).json(data.rows)
    })
    .catch(err => {
        res.status(500).send({  message: "Error en la consulta del por ciudad =" + nombre_ciudad + " error:" + err});
    });
}

module.exports = {
    getByPais,
    getByEstado,
    getByCiudad
}