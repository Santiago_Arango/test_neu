-- luego de correr el metodo que generan las tablas e implementar el import de los datos, recorrer este script que consolida la indormacion en una tabla 

INSERT INTO public.consolidados(
	nombre_ciudad, nombre_estado, nombre_pais, poblacion)
	select ciudades.nombre, estados.nombre,paises.nombre, ciudades.poblacion from ciudades 
			inner join estados on ciudades.id_estado = estados.id_estado
			inner join paises on estados.id_pais = paises.id_pais
