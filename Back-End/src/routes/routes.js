const { Router } = require('express')
const router = Router()

const {getByPais, getByCiudad, getByEstado} = require('../controllers/controller')

router.get('/pais/:nombre_pais',getByPais)
router.get('/ciudad/:nombre_ciudad',getByCiudad)
router.get('/estado/:nombre_estado',getByEstado)

module.exports = router